#include <stdio.h>
#include <stdlib.h>

int main()
{
    int qtd = 0;
    while (qtd < 1) {
        printf("Informe o numero de equacoes: ");
        scanf("%d", &qtd);
        if (qtd < 1) {
            printf("Informe um numero valido! (maior que 0)\n");
        }
    }
    
    int fun[qtd][3], num;
    char letra[3] = {'x', 'y', 'z'};
    
    for (int i = 0; i < qtd;i++ ) {
        system("clear");
        printf("Valores da funcao %d\n", i+1);
        for (int j = 0;j < 3;j++) {
            printf("Informe o valor de %c: ", letra[j]);
            scanf("%d", &num);
            fun[i][j] = num;
        }
        printf("\n");
    }
    
    system("clear");
    
    for (int j = 0;j < 3;j++) {
        printf("| %c ", letra[j]);
        if (j == 2) {
            printf("|\n");
        }
    }
    
    for (int i = 0; i < qtd;i++ ) {
        for (int j = 0;j < 3;j++) {
            printf("| %d ", fun[i][j]);
            if (j == 2) {
                printf("|\n");
            }
        }
    }
    
    for (int i = 0;i < qtd;i++) {
        
        for (int j = 0;j < 3;j++) {
            if (j < i) {
                
                for (int t = j+1;t < 3;t++) {
                    if (j > 0) {
                        fun[i][t] -= (fun[i][j] / fun[i-1][j]) * fun[i-1][t];
                    } else {
                        fun[i][t] -= (fun[i][j] / fun[0][j]) * fun[0][t];
                    }
                }
                fun[i][j] -= (fun[i][j] / fun[0][j]) * fun[0][j];
            }
        }
    }
    
    system("clear");
    
    for (int i = 0; i < qtd;i++ ) {
        for (int j = 0;j < 3;j++) {
            printf("|  %d  ", fun[i][j]);
            if (j == 2) {
                printf("|\n");
            }
        }
    }
    
    return 0;
}