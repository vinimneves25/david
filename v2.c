#include <stdio.h>
#include <stdlib.h>

int main()
{
    int qtd = 0;
    while (qtd < 1) {
        printf("Quantas equações teremos nesse sistema? ");
        scanf("%d", &qtd);
        if (qtd < 1) {
            printf("Informe um numero valido! (maior que 0)\n");
        }
    }
    
    float fun[qtd][qtd];
    int num;
    char letra[10] = {'x', 'y', 'z', 'k', 'l', 'b', 'c', 'a', 'd', 'e'}; // Letras das variaveis em ordem, sao lidas dinamicamente, pode altera-las sem prejudicar o sistema
    
    for (int i = 0; i < qtd;i++ ) {
        system("clear");
        printf("Valores da funcao %d\n", i+1);
        for (int j = 0;j < qtd;j++) {
            printf("Informe o valor de %c: ", letra[j]);
            scanf("%d", &num);
            fun[i][j] = num;
        }
        printf("\n");
    }
    
    system("clear");
    
    for (int j = 0;j < qtd;j++) {
        printf("| %c ", letra[j]);
        if (j == qtd-1) {
            printf("|\n");
        }
    }
    
    for (int i = 0; i < qtd;i++ ) {
        for (int j = 0;j < qtd;j++) {
            printf("| %f ", fun[i][j]);
            if (j == qtd-1) {
                printf("|\n");
            }
        }
    }
    system("clear");
    int meio;
    if (qtd%2 > 0) {
        meio = (qtd / 2) + 1;
    } else {
        meio = qtd / 2;
    }
    
    for (int j = 0;j < meio;j++) {
        for (int i = j+1;i < qtd;i++) {
            for (int t = 0;t < qtd;t++) {
                if (t != j) {
                    fun[i][t] -= (fun[i][j] / fun[j][j]) * fun[j][t];
                }
            }
            fun[i][j] -= (fun[i][j] / fun[j][j]) * fun[j][j];
        }
    }
    
    system("clear");
    
    for (int i = 0; i < qtd;i++ ) {
        for (int j = 0;j < qtd;j++) {
            if (fun[i][j] != fun[i][j]) { // Verifica se o return eh 0.0 / 0.0 (NaN)
                printf("|  0  ");
            } else {
                printf("|  %.0f  ", fun[i][j]); // %.0f eh tipo float (decimal) com duas casas apos a virgula
            }
            if (j == qtd-1) {
                printf("|\n");
            }
        }
    }
    
    return 0;
}